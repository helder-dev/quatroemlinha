/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;



/**
 * @author Helder Oliveira 
 * @author Rui Rafael
 */
public class QuatroEmLinhaCampeonato {
    private Jogador[][] calendario;
    private int numJogos;
    private Jogador jogador1;
    private Jogador jogador2;
    
    /**
     * Construtor da classe QuatroEmLinhaCampeonato, que recebe como parâmetro de entrada
     * os jogadores que vão participar no campeonato, e cria o calendário da seguinte forma:
     * Na 1º e 2º coloca os nomes dos jogadores que vão jogar. A 3º e 4ºcoluna vão servir para
     * determinar se os jogos já foram efetuados ou não. Na 3º coluna de todos os jogos, coloca "NULL"
     * indicando que o jogo ainda não foi efetuado.
     * @param jogadoresCampeonato Jogadores que vão participar no campeonato
     */
    public QuatroEmLinhaCampeonato (Jogador[] jogadoresCampeonato){
        int z = 0 ; //index do vetor calendario
        for (int o = (jogadoresCampeonato.length - 1) ; o > 0 ; o--){
            numJogos= numJogos + o; // calculo do número de jogadores
        }

        calendario = new Jogador[numJogos][4];
        
        for(int i = 0 ; i < jogadoresCampeonato.length ; i ++ ){
            for (int j = 0 ; j < jogadoresCampeonato.length ; j++){	
                if (i!=j && i == 0 ){
                    calendario[z][0] = jogadoresCampeonato[i];
                    calendario[z][1] = jogadoresCampeonato[j];
                    calendario[z][2] = null;
                    z++;
                }
                if ((i != j) && ( i != 0 ) && (j > i)){  
                    calendario[z][0] = jogadoresCampeonato[i];
                    calendario[z][1] = jogadoresCampeonato[j];
                    calendario[z][2] = null;
                    z++;
                }
            } 
        }
    }
    
    /**
     * Construtor da classe QuatroEmLinhaCampeonato utilizado para carregar.
     * @param calendario
     * @param numJogos 
     */
    public QuatroEmLinhaCampeonato (Jogador[][] calendario, int numJogos){
        this.calendario = calendario;
        this.numJogos = numJogos;
    }
    
    /**
     * Método para devolver calendário
     * @return 
     */
    public Jogador[][] getCalendario() {
        return this.calendario;
    }
    
    /**
     * Método para devolver numero de jogos.
     * @return 
     */
    public int getNumJogos() {
        return this.numJogos;
    }
    
    /**
     * Vai ao calendário, e pesquisa na 3º coluna(que é a coluna que é preenchida com o jogador, depois do jogo terminar) se
     * está null ou não. Se estiver null, significa que esse jogo não foi efetuado, em seguida vai á coluna 1 e 2
     * buscar os respetivos jogadores desse jogo.
     * @return Os jogadores, do jogo a efetuar.
     */
    public Jogador[] getJogadoresCampeonato(){
         for(int i = 0 ; i < numJogos ; i++){
            if(calendario[i][2] == null){
                this.jogador1 = calendario[i][0];
                this.jogador2 = calendario[i][1];
                break;
            }
          }
         Jogador[] jogadores = new Jogador[2];
         jogadores[0] = this.jogador1;
         jogadores[1] = this.jogador2;
         
        return jogadores;
    }

    /**
     * Este método é responsável por atualizar o calendario e atribuir derrotas,vitórias ou empates. Funciona da seguinte maneira:
     * Recebe uma string , que se o seu valor for "Empate", atribuí empate aos dois jogadores que recebeu como parâmetro de entrada.
     * Se a string que receber for "Vencedor" , atribuí uma vitória ao Vencedor e uma derrota ao Derrotado. 
     * Atualiza o calendário da seguinte forma : vai á 3º e 4º coluna do calendário , e coloca os nomes dos dois jogadores
     * que recebeu como parâmetro.
     * @param winner Jogador vencedor
     * @param loser Jogador derrotado
     * @param estadoFinalJogo String que determina vai determinar se houve empate ou não. 
     */
    public void atualizarCalendario(Jogador winner, Jogador loser, String estadoFinalJogo){
        for (int i = 0 ; i <(calendario.length) ; i++){
            if(estadoFinalJogo == "Vencedor"){
                if (calendario[i][0].getIdJogador() == this.jogador1.getIdJogador() && calendario[i][1].getIdJogador() == this.jogador2.getIdJogador()){
                    calendario[i][2] = winner;
                    winner.setNumJogosGanhos(1);
                    calendario[i][3] = loser;
                    loser.setNumDerrotas(1);
                    break;
                }
            }else if(estadoFinalJogo == "Empate"){
                if (calendario[i][0].getIdJogador() == this.jogador1.getIdJogador() && calendario[i][1].getIdJogador() == this.jogador2.getIdJogador()){
                    calendario[i][2] = winner;
                    winner.setNumEmpates(1);
                    calendario[i][3] = loser;
                    loser.setNumEmpates(1);
                    break;
                }
            }
        }
    }
    
    /**
     * Vai ao calendário e verifica a 3º coluna do calendário, esta coluna é uma das colunas que é preenchida com os jogadores
     * depois do jogo terminar. Este método vai incrementando 1 á variável contadorJogosTerminados , sempre que encontra a 3º coluna do 
     * calendário preenchida. Vai comparando o valor da variável contador de Jogos Terminados ao tamanho do Calendário.
     * @return true se o valor da variável contadorJogosTerminados for igual ao tamanho do calendário, false se o contrário acontecer.
     */
    public boolean fimCampeonato(){
        boolean fimCampeonato = false;
        int contadorJogosTerminados = 0 ;
        for(int i = 0 ; i < calendario.length ; i++){
            if (calendario[i][2] != null ){
               contadorJogosTerminados = contadorJogosTerminados + 1;
            }
        }
        if(contadorJogosTerminados == calendario.length){
            fimCampeonato = true;
        }
        
        return fimCampeonato;
    }
    
    /**
     * Vai ao calendario do campeonato, e vai á coluna dos vencedores(3º coluna) e vai buscar o total de Pontos que eles têm.
     * Caso o total de pontos do jogador, for maior que o jogador com o maior número de pontos, então esse jogador passa a ser
     * o jogador Vencedor do campeonato. Caso haja um empate nos pontos, faz outro teste para verificar quais dos jogadores têm menos jogadas,
     * o que tiver menos jogadas passa a ser o jogador Vencedor do campeonato.
     * @return jogador Vencedor do Campeonato
     */
    public Jogador determinarVencedorCampeonato(){
        int numPontos = 0;
        Jogador jogadorVencedor = calendario[0][2];
        for(int i = 0 ; i < calendario.length ; i++){
            if(calendario[i][2].getTotalPontosCampeonato() > numPontos ){
                numPontos = calendario[i][2].getTotalPontosCampeonato();
                jogadorVencedor = calendario[i][2];
            } else if (calendario[i][2].getTotalPontosCampeonato() == numPontos){
                if(calendario[i][2].getTotalJogadasCampeonato() < jogadorVencedor.getTotalJogadasCampeonato()){
                    jogadorVencedor = calendario[i][2];
                }
            }
        }
        return jogadorVencedor;
    }  
}