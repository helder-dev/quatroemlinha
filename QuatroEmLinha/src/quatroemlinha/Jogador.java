/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;

/**
 * @author Hélder Oliveira
 * @author Rui Rafael
 */
public class Jogador {
    private int idJogador;
    private String nome;
    private int idade;
    private int numJogosGanhos = 0;
    private int numDerrotas = 0;
    private int numEmpates = 0;
    private int numCampeonatosGanhos = 0;
    private int totalPontosCampeonato = 0;
    private int totalJogadasCampeonato = 0;
 
    /**
     * Contrutor para um novo Jogador.
     * @param nome
     * @param idade
     * @param numJogosGanhos
     * @param numCampeonatosGanhos 
     */
    public Jogador(int idJogador, String nome, int idade, int numJogosGanhos, int numDerrotas, int numEmpates, int numCampeonatosGanhos, int totalPontosCampeonato, int totalJogadasCampeonato) {
        this.idJogador = idJogador;
        this.nome = nome;
        this.idade = idade;
        this.numJogosGanhos = numJogosGanhos;
        this.numDerrotas = numDerrotas;
        this.numEmpates = numEmpates;
        this.numCampeonatosGanhos = numCampeonatosGanhos;
        this.totalPontosCampeonato = totalPontosCampeonato;
        this.totalJogadasCampeonato = totalJogadasCampeonato;
    }    
    
    /**
     * Método para definir o Nome do Jogador.
     * @param nome Nome do Jogador.
     */
    public void setNome(String nome){
        this.nome = nome;
    }
    /**
     * Método para definr a idade do jogador 
     * @param idade Idade do Jogador.
     */
    public void setIdade(int idade){
        this.idade = idade; 
    }
    
    /**
     * Adicionar Jogos ganhos
     * @param adicionarJogo Adiciona 1 ao número de jogos ganhos
     */
    public void setNumJogosGanhos(int adicionarJogo){
        this.numJogosGanhos = this.numJogosGanhos + adicionarJogo;
    }
    
    /**
     * Adicionar Derrotas
     * @param adicionarDerrota Adiciona 1 ao número de jogos perdidos
     */
    public void setNumDerrotas(int adicionarDerrota){
        this.numDerrotas = this.numDerrotas + adicionarDerrota;
    }
    
    /**
     * Adicionar Empates
     * @param adicionarEmpate Adiciona 1 ao número de jogos empatados
     */
    public void setNumEmpates(int adicionarEmpate){
        this.numEmpates = this.numEmpates + adicionarEmpate;
    }
    
    /**
     * Adicionar Número de Campeonatos Ganhos
     * @param adicionarCampeonatoGanho Adiciona 1 ao número de Campeonatos ganhos
     */
    public void setNumCampeonatosGanhos(int adicionarCampeonatoGanho){
        this.numCampeonatosGanhos = adicionarCampeonatoGanho;
    }

    /**
     * Adicionar número de jogadas durante o campeonato
     * @param adicionarJogadaCampeonato Adicionar 1 ao número de Jogadas
     */
    public void setTotalJogadasCampeonato(int adicionarJogadaCampeonato){
        this.totalJogadasCampeonato = this.totalJogadasCampeonato + adicionarJogadaCampeonato;
    }

    /**
     * @return Retorna a idade do jogador
     */
    public int getIdade(){
        return idade;
    }
    
    /**
     * @return o ID do Jogador
     */
    public int getIdJogador() {
        return idJogador;
    }
    
    /**
     * @return Retorna o Nome do Jogador
     */
    public String getNome(){
        return nome;
    }
    
    /**
     * @return Número de Jogos Ganhos
     */
    public int getNumJogosGanhos(){
        return this.numJogosGanhos;
    }
    
    /**
     * @return Número de Derrotas
     */
    public int getNumDerrotas(){
        return this.numDerrotas;
    }
    
    /**
     * @return Número de Empates 
     */
    public int getNumEmpates(){
        return this.numEmpates;
    }
    
    /**
     * @return Número de Campeonatos ganhos
     */
    public int getNumCampeonatosGanhos(){
        return this.numCampeonatosGanhos;
    }
    
    /**
     * @return Total de Pontos do Jogador no Campeonato 
     */
    public int getTotalPontosCampeonato(){
        return (3 * this.numJogosGanhos) + (1 * this.numEmpates) + (0 * this.numDerrotas);
    }
    
    /**
     * @return Total de Jogadas do Jogador no Campeonato 
     */
    public int getTotalJogadasCampeonato(){
        return this.totalJogadasCampeonato;
    }  
}