/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;

/**
 *
 * @author Hélder Oliveira
 * @author Rui Rafael
 */
public class QuatroEmLinhaJogo {
    private int numJogo;
    
    private char[][] tabuleiro;
    
    private Jogador[] jogadores;
    private int idxJogador;
    
    public final static int LINHAS = 6; // Número de Linhas do Tabuleiro
    public final static int COLUNAS = 7; // Número de Colunas do Tabuleiro
    private final static char[] FICHAS = {'X', 'O'}; // Representação das Fichas no Tabuleiro
    
    /**
     * Construtor que inicializa do ínicio.
     * 
     * @param jogadores 
     */
    public QuatroEmLinhaJogo(int numJogo, Jogador[] jogadores) {
        this.numJogo = numJogo;
        
        char[][] initTabuleiro = new char[LINHAS][COLUNAS];
        for (int i = 0; i < LINHAS; i++) {
            for (int j = 0; j < COLUNAS; j++) {
                initTabuleiro[i][j] = '.';
            }
        }
        
        this.tabuleiro = initTabuleiro;
        this.jogadores = jogadores;
        this.idxJogador = 0;
    }
    
    /**
     * Construtor que inicializa jogo em andamento.
     * @param numJogo
     * @param tabuleiro
     * @param jogadores
     * @param idxJogador 
     */
    public QuatroEmLinhaJogo(int numJogo, char[][] tabuleiro, Jogador[] jogadores, int idxJogador) {
        this.numJogo = numJogo;
        this.tabuleiro = new char[LINHAS][COLUNAS];
        for (int i = 0; i < LINHAS; i++) {
            for (int j = 0; j < COLUNAS; j++) {
                this.tabuleiro[i][j] = tabuleiro[i][j];
            }
        }
        
        this.jogadores = jogadores;
        this.idxJogador = idxJogador;
    }
    
    /**
     * @return O número do jogo que está a ser efetuado
     */
    public int getNumJogo() {
        return numJogo;
    }
    
    /**
     * @return O tabuleiro do jogo
     */
    public char[][] getTabuleiro() {
        return tabuleiro;
    }
    
    /**
     * @return Os jogadores do jogo que está a ser efetuado 
     */
    public Jogador[] getJogadores() {
        return jogadores;
    }
    
    /**
     * @return O jogador do jogo que está a ser efetuado, 
     */
    public Jogador getJogador() {
        return jogadores[idxJogador];
    }
    
    /**
     * @return o index do jogador que está a jogar
     */
    public int getIdxJogadores() {
        return idxJogador;
    }

    /**
     * Altera o index do jogador, para o index do próximo jogador que vai jogar.
     */
    public void setProximoJogador() {
        this.idxJogador = 1 - idxJogador;
    }
    
    /**
     * Método para realizar uma jogada.
     * @param coluna 
     */
    public void aplicarJogada(int coluna) {
        int linha = 0;
        while (tabuleiro[linha][coluna - 1] != '.' && linha < LINHAS) {
            linha++;
        }

        tabuleiro[linha][coluna - 1] = FICHAS[idxJogador]; // Marca na matriz a jogada que o jogador fez.
    }
    
    /**
     * Verifica se o jogo chegou ao fim.
     * Valida se o tabuleiro está cheio ou se existem 4 fichas em linha.
     * @return True se o jogo acabou e False se não acabou
     */
    public boolean isGameOver() {
        boolean gameOver = false;
        if (isTabuleiroCheio() || analisaQuatroEmLinha()) {
            gameOver = true;
        }
        
        return gameOver;
    }
    
    /**
     * Verifica se o tabuleiro já está cheio.
     * Para isso verificamos se a última linha do tabuleiro está completamente
     * preenchida.
     * @return True se o tabuleiro já estiver cheio, False se não estiver.
     */
    public boolean isTabuleiroCheio() {
        boolean cheio = true;
        for (int j = 0; j < COLUNAS; j++) {
            if (tabuleiro[LINHAS - 1][j] == '.') {
                cheio = false;
            }
        }
        return cheio;
    }
    
    /**
     * Verifica se existem 4 fichas em linha nas várias posições (vertical, horizontal ou diagonal).
     * @return False, para estar sempre a verificar se existem 4 fichas em linha
     */
    public boolean analisaQuatroEmLinha() {
        boolean quatroEmLinha = false;
        for (int i = 0; i <= (LINHAS - 1) && quatroEmLinha == false; i++) {
            if (i < (LINHAS - 4)) {
                for (int j = 0; j <= (COLUNAS - 1) && quatroEmLinha == false; j++) {
                    quatroEmLinha = isQuatroEmLinha(i,j);
                }
            } else {
                for (int j = 0; j <= (COLUNAS - 4) && quatroEmLinha == false; j++) {
                    quatroEmLinha = isQuatroEmLinha(i,j);
                }
            }
        }
        return quatroEmLinha;
    }
    
    /**
     * Faz os testes se existem 4 fichas iguais na vertical, horizontal e diagonal
     * @param linha
     * @param coluna
     * @return True se existirem 4 fichas iguais na vertical, horizontal e diagonal. False se o anterior não se verificar
     */
    public boolean isQuatroEmLinha(int linha, int coluna) {
        boolean quatroEmLinha = false;
        if (linha < (LINHAS - 4)) {
            // VERTICAL
            quatroEmLinha = true;
            for (int i = linha; i <= (linha + 3); i++) {
                if (tabuleiro[i][coluna] != FICHAS[1 - idxJogador]) {
                    quatroEmLinha = false;
                    break;
                }
            }
            
            // DIAGONAL (\)
            if (coluna < (COLUNAS - 3) && quatroEmLinha == false) {
                quatroEmLinha = true;
                for (int i = linha, j = coluna; i < linha + 3; i++, j++) {
                    if (tabuleiro[i][j] != FICHAS[1 - idxJogador]) {
                        quatroEmLinha = false;
                    }
                }
            }
        }
        
        if (coluna <= (COLUNAS - 3) && quatroEmLinha == false) {
            // HORIZONTAL
            quatroEmLinha = true;
            for (int j = coluna; j <= (coluna + 3); j++) {
               if (tabuleiro[linha][j] != FICHAS[1 - idxJogador]) {
                   quatroEmLinha = false;
                   break;
               }
            }
            
            // DIAGONAL (/)
            if (linha > 2 && quatroEmLinha == false) {
                quatroEmLinha = true;
                for (int i = linha , j = coluna; j <= (coluna + 3); i--, j++) {
                    if (tabuleiro[i][j] != FICHAS[1 - idxJogador]) {
                        quatroEmLinha = false;
                        break;
                    }
                }
            }
        }

        return quatroEmLinha;
    }
}
