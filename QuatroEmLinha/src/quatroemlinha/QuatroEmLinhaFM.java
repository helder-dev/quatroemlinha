/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.RandomAccessFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author helder.oliveira
 */
public class QuatroEmLinhaFM extends QuatroEmLinhaUI {
    private String tempPath;
    private Jogador[][] returnCalendarioJogadores;
    
    
    /**
     * Construtor da classe QuatroEmLinhaFM 
     * A caracteristica tempPath fica com o caminho para o diretório temp
     */
    public QuatroEmLinhaFM() {
        this.tempPath = System.getProperty("java.io.tmpdir");
    }
    
    /**
     * Método para verificar se o ficheiro existe.
     * @param nomeFicheiro
     * @return 
     */
    public boolean ficheiroExiste(String nomeFicheiro) {
	  File f = new File(tempPath + nomeFicheiro);
	  return f.exists();
    }
  
    /**
     * Método para criar um ficheiro.
     * @param nomeFicheiro 
     */
    public void criarFicheiro(String nomeFicheiro) {
        try {
            File f = new File(tempPath +nomeFicheiro);
            f.createNewFile();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao criar o ficheiro.");
        }
    }
 
    /**
     * Método para verificar se o jogador com o ID introduzido, já existe nos ficheiros.
     * @param idJogador
     * @return True se o jogador já existir nos ficheiros, False se não existir
     */
    public boolean jogadorExiste(int idJogador) {
        boolean existe = false;
        
        if (ficheiroExiste("Jogadores.txt")) {
            try {
                FileReader fich = new FileReader(tempPath + "Jogadores.txt");
                BufferedReader lerFich = new BufferedReader(fich);
                
                String linha = lerFich.readLine();
                while (linha != null) {
                    if (linha.split(":")[0] != null && (Integer.parseInt(linha.split(":")[0]) == idJogador)) {
                        existe = true;
                        break;
                    }
                    
                    linha = lerFich.readLine();
                }

                fich.close();
            } catch (IOException e) {
                super.setMensagemErro("Erro ao verificar se o jogador já existe.");
            }
        }
        
        return existe;
    }
    
    /**
     * @param idJogador 
     * @return O Jogador com o ID que foi introduzido.
     */
    public Jogador getJogador(int idJogador) {
        if (ficheiroExiste("Jogadores.txt")) {
            try {
                FileReader fich = new FileReader(tempPath + "Jogadores.txt");
                BufferedReader lerFich = new BufferedReader(fich);
                
                String linha = lerFich.readLine();
                while (linha != null) {
                    if (linha.split(":")[0] != null && (Integer.parseInt(linha.split(":")[0]) == idJogador)) {
                        int numJogosGanhos = Integer.parseInt(linha.split(":")[3]),
                            numDerrotas = Integer.parseInt(linha.split(":")[4]),
                            numEmpates = Integer.parseInt(linha.split(":")[5]),
                            numCampeonatosGanhos = Integer.parseInt(linha.split(":")[6]),
                            totalPontosCampeonato = Integer.parseInt(linha.split(":")[7]),
                            totalJogadasCampeonato = Integer.parseInt(linha.split(":")[8]);
                        
                        return new Jogador(Integer.parseInt(linha.split(":")[0]), linha.split(":")[1], Integer.parseInt(linha.split(":")[2]), numJogosGanhos, numDerrotas, numEmpates, numCampeonatosGanhos, totalPontosCampeonato, totalJogadasCampeonato);
                    }
                    
                    linha = lerFich.readLine();
                }

                lerFich.close();
            } catch (IOException e) {
                super.setMensagemErro("Erro ao verificar se o jogador já existe.");
            }
        }
        
        return null;
    }
    
    /**
     * Método para guardar o jogador, no ficheiro "Jogadores.txt"
     * @param jogador 
     */
    public void guardarJogador(Jogador jogador) {
        if (!ficheiroExiste("Jogadores.txt")) {
            criarFicheiro("Jogadores.txt");
        }
        
        try {
            FileWriter fich = new FileWriter(tempPath + "Jogadores.txt", true);
            BufferedWriter escreverFich = new BufferedWriter(fich);
            
            escreverFich.write(jogador.getIdJogador() + ":" + jogador.getNome() + ":" + jogador.getIdade() + ":" + jogador.getNumJogosGanhos() + ":" + jogador.getNumDerrotas() + ":" + jogador.getNumEmpates() + ":" + jogador.getNumCampeonatosGanhos() + ":" + jogador.getTotalJogadasCampeonato() + ":" + jogador.getTotalPontosCampeonato());
            escreverFich.newLine();

            escreverFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogador no ficheiro.");
        }
    }
    
    /**
     * Métodos para atulizar atributos do jogador nos ficheiros.
     * @param jogador 
     */
    public void atualizarJogador(Jogador jogador) {
        try {
            FileReader fichR = new FileReader(tempPath + "Jogadores.txt");
            BufferedReader lerFich = new BufferedReader(fichR);
            
            FileWriter fichWS = new FileWriter(tempPath + "Jogadores.txt",false);
            BufferedWriter escreverFichS = new BufferedWriter(fichWS);
            
            String linha = lerFich.readLine();
            String linhaAux;
            String linhaNew;
            String[] dados;
            
            while (linha != null) {
                dados = linha.split(":");
                if (dados[0].equals(Integer.toString(jogador.getIdJogador()))) {
                    linhaAux = linha;
                    linhaNew = jogador.getIdJogador() + ":" + jogador.getNome() + ":" + jogador.getIdade() + ":" + jogador.getNumJogosGanhos() + ":" + jogador.getNumDerrotas() + ":" + jogador.getNumEmpates() + ":" + jogador.getNumCampeonatosGanhos() + ":" + jogador.getTotalJogadasCampeonato() + ":" + jogador.getTotalPontosCampeonato();
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    escreverFichS.close();
                    break;
                }
                
                linha = lerFich.readLine();
            }
            lerFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
    }
    
    
    /** 
     * @return Número de jogos guardados no ficheiro
     */
    public int getNumJogo() {
        int numJogo = 1;
        
         if (!ficheiroExiste("Jogos.txt")) {
            return numJogo;
        }
         
        try {
            FileReader fichR = new FileReader(tempPath + "Jogos.txt");
            BufferedReader lerFich = new BufferedReader(fichR);

            String linha = lerFich.readLine();
            while (linha != null) {
                if (linha.split(" ")[0].equals("#")) {
                    numJogo++;
                }

                linha = lerFich.readLine();
            }
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
        
        return numJogo;
    }
    
    /**
     * Método utilizado para guardar o Jogo no ficheiro.
     * @param jogo Objeto Jogo
     */
    public void guardarJogo(QuatroEmLinhaJogo jogo) {
        if (!ficheiroExiste("Jogos.txt")) {
            criarFicheiro("Jogos.txt");
        }
        
        try {
            boolean exists = false;
            
            char[][] tabuleiro = jogo.getTabuleiro();
            
            FileReader fichR = new FileReader(tempPath + "Jogos.txt");
            BufferedReader lerFich = new BufferedReader(fichR);
            
            FileWriter fichWS = new FileWriter(tempPath + "Jogos.txt");
            BufferedWriter escreverFichS = new BufferedWriter(fichWS);
            
            String linha = lerFich.readLine();
            String linhaAux;
            String linhaNew;
            while (linha != null) {
                if (linha.equals("# JOGO " + jogo.getNumJogo())) {
                    lerFich.readLine();
                    lerFich.readLine();
                    
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[0][0] + " " + tabuleiro[0][1] + " " + tabuleiro[0][2] + " " + tabuleiro[0][3] + " " + tabuleiro[0][4] + " " + tabuleiro[0][5] + " " + tabuleiro[0][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[1][0] + " " + tabuleiro[1][1] + " " + tabuleiro[1][2] + " " + tabuleiro[1][3] + " " + tabuleiro[1][4] + " " + tabuleiro[1][5] + " " + tabuleiro[1][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[2][0] + " " + tabuleiro[2][1] + " " + tabuleiro[2][2] + " " + tabuleiro[2][3] + " " + tabuleiro[2][4] + " " + tabuleiro[2][5] + " " + tabuleiro[2][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[3][0] + " " + tabuleiro[3][1] + " " + tabuleiro[3][2] + " " + tabuleiro[3][3] + " " + tabuleiro[3][4] + " " + tabuleiro[3][5] + " " + tabuleiro[3][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[4][0] + " " + tabuleiro[4][1] + " " + tabuleiro[4][2] + " " + tabuleiro[4][3] + " " + tabuleiro[4][4] + " " + tabuleiro[4][5] + " " + tabuleiro[4][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    linhaAux = lerFich.readLine();
                    linhaNew = tabuleiro[5][0] + " " + tabuleiro[5][1] + " " + tabuleiro[5][2] + " " + tabuleiro[5][3] + " " + tabuleiro[5][4] + " " + tabuleiro[5][5] + " " + tabuleiro[5][6];
                    escreverFichS.write(linhaAux.replace(linhaAux, linhaNew));
                    
                    escreverFichS.close(); 
                    break;
                }

                linha = lerFich.readLine();
            }
            
            if (exists == false) {
                FileWriter fichW = new FileWriter(tempPath + "Jogos.txt", true);
                BufferedWriter escreverFich = new BufferedWriter(fichW);
                
                escreverFich.write("# JOGO " + jogo.getNumJogo());
                escreverFich.newLine();
            
                Jogador[] jogadores = jogo.getJogadores();

                escreverFich.write(Integer.toString(jogadores[0].getIdJogador()));
                escreverFich.newLine();
                escreverFich.write(Integer.toString(jogadores[1].getIdJogador()));
                escreverFich.newLine();

                for (int i = 0; i < jogo.LINHAS; i++) {
                   escreverFich.write(tabuleiro[i][0] + " " + tabuleiro[i][1] + " " + tabuleiro[i][2] + " " + tabuleiro[i][3] + " " + tabuleiro[i][4] + " " + tabuleiro[i][5] + " " + tabuleiro[i][6]);
                   escreverFich.newLine();
                }

                escreverFich.write(Integer.toString(jogo.getJogador().getIdJogador()));
                escreverFich.newLine();

                escreverFich.close();           
            }
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
    }
    
    /**
     * @return String com o nome dos jogos guardados
     */
    public String jogosGuardados() {
        if (!ficheiroExiste("Jogos.txt")) {
            return "";
        }
        
        String jogos = "";
        try {
            FileReader fichR = new FileReader(tempPath + "Jogos.txt");
            BufferedReader lerFich = new BufferedReader(fichR);
            
            
            String linha = lerFich.readLine();
            while (linha != null) {
                if (linha.split(" ")[0].equals("#")) {
                    if (jogos == "") {
                        jogos = linha.replace("# ", "");
                    } else {
                        jogos = jogos + ";" + linha.replace("# ", "");
                    }
                }

                linha = lerFich.readLine();
            }
            
            lerFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
        
        return jogos;
    }
    
    /**
     * @param nomeJogo
     * @return O Jogo escolhido pelo utilizador
     */
    public QuatroEmLinhaJogo getJogoGuardado(String nomeJogo) {
        int numJogo = 0;
        char[][] tabuleiro = new char[6][7];
        Jogador[] jogadores = new Jogador[2];
        int idxJogador = 0;
        
        try {
            FileReader fichR = new FileReader(tempPath + "Jogos.txt");
            BufferedReader lerFich = new BufferedReader(fichR);
            
            String linha = lerFich.readLine();
            while (linha != null) {
                if (linha.replace("# ", "").equals(nomeJogo)) {
                    numJogo = Integer.parseInt(linha.split(" ")[2]);
                    
                    jogadores[0] = getJogador(Integer.parseInt(lerFich.readLine()));
                    jogadores[1] = getJogador(Integer.parseInt(lerFich.readLine()));
                    
                    String fichas = "";
                    for (int i = 0; i < 6; i++) {
                        fichas = lerFich.readLine();
                        
                        for (int j = 0; j < fichas.split(" ").length; j++) {
                            tabuleiro[i][j] = fichas.split(" ")[j].charAt(0);
                        }
                    }
                    
                    if (jogadores[1].getIdJogador() == Integer.parseInt(lerFich.readLine())) {
                        idxJogador = 1;
                    }
                    
                    break;
                }

                linha = lerFich.readLine();
            }
            
            lerFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
        
        return new QuatroEmLinhaJogo(numJogo, tabuleiro, jogadores, idxJogador);
    }
 
    /**
     * Método para guardar campeonato.
     * @param camp 
     */
    public void guardarCampeonato(QuatroEmLinhaCampeonato camp) {
        if (!ficheiroExiste("Campeonato.txt")) {
            criarFicheiro("Campeonato.txt");
        }
        
        try {
            FileWriter fichW = new FileWriter(tempPath + "Campeonato.txt", true);
            BufferedWriter escreverFich = new BufferedWriter(fichW);

            escreverFich.write(Integer.toString(camp.getNumJogos()));
            escreverFich.newLine();
            
            Jogador[][] calendario = camp.getCalendario();

            for (int i = 0; i < calendario.length; i++) {
                escreverFich.write(calendario[i][0].getIdJogador() + ";" + calendario[i][1].getIdJogador() + ";" + ((calendario[i][2] == null) ? "" : calendario[i][2].getIdJogador()) + ";" + ((calendario[i][3] == null) ? "" : calendario[i][3].getIdJogador()));
                escreverFich.newLine();
            }

            escreverFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao gravar o jogo no ficheiro.");
        }
    }
    
    /**
     * Método para devolver campeonato guardado
     * @return 
     */
    public QuatroEmLinhaCampeonato getCampeonatoGuardado() {
        Jogador[][] calendario = new Jogador[99][4];
        int numJogos = 0;
        
        try {
            FileReader fichR = new FileReader(tempPath + "Campeonato.txt");
            BufferedReader lerFich = new BufferedReader(fichR);
            
            String linha = lerFich.readLine();
            if (linha != null) {
                numJogos = Integer.parseInt(linha);
            }
            calendario = new Jogador[numJogos][4];
            
            linha = lerFich.readLine();
            String[] dados;
            int count = 0;
            
            while (linha != null) {
                dados = linha.split(";");

                calendario[count][0] = getJogador(Integer.parseInt(dados[0]));
                calendario[count][1] = getJogador(Integer.parseInt(dados[1]));
                if (dados.length > 2) {
                    calendario[count][2] = getJogador(Integer.parseInt(dados[2]));
                    if (dados.length > 3) {
                        calendario[count][3] = getJogador(Integer.parseInt(dados[3]));
                    } else {
                        calendario[count][3] = null;
                    }
                } else {
                    calendario[count][2] = null;
                    calendario[count][3] = null;
                }

                count++;

                linha = lerFich.readLine();
            }
            
            lerFich.close();
        } catch (IOException e) {
            super.setMensagemErro("Erro ao ler o campeonato do ficheiro.");
        }
       
        return new QuatroEmLinhaCampeonato(calendario, numJogos);
    }
    
    
   
    
    

    
  
    
   
    
   
    
    
    
}
        
        