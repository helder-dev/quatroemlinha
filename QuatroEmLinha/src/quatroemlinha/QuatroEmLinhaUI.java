/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hélder Oliveira
 * @author Rui Rafael
 */
public class QuatroEmLinhaUI {
    
    private static final Scanner scanner = new Scanner(System.in);
    private static final BufferedReader breader = new BufferedReader(new InputStreamReader(System.in));
    
    /*
     * @return A opção escolhida pelo utilizador  
     */
    public int menuPrincipalV2() {
        System.out.println("Selecione a opção: ");
        System.out.println("0 - Sair");
        System.out.println("1 - Modo 1vs1 ");
        System.out.println("2 - Modo Campeonato");
        System.out.println("3 - Consultar Ranking");

        return getInt("");
    }
    
    /**
     * @return A opção escolhida pelo utilizador
     */
    public int menu1vs1V2() {
        System.out.println("Selecione a opção: ");
        System.out.println("1 - Novo Jogo");
        System.out.println("2 - Continuar Jogo");
        System.out.println("3 - Voltar ao Menu");
        
        return getInt("");
    }
    
    /**
     * Método para mostrar o menuCampeonato, onde oferece as seguintes possibilidade : "1 - Novo Campeonato" ; "2 - Continuar Campenato" ;
     * "3 - Voltar ao Menu"
     * @param ui Objeto User Intercace
     * @return opcao escolhida pelo utilizador
     */
    public static int menuCampeonato(QuatroEmLinhaUI ui){
        System.out.println("1 - Novo Campeonato");
        System.out.println("2 - Continuar Campeonato");
        System.out.println("3 - Voltar ao Menu");
        int opcao = ui.getInt("");
        return opcao;
    }
    
    /**
     * @param jogos String com o nome de todos os jogos guardados no ficheiro "Jogos.txt"
     * @return O nome do jogo escolhido pelo utilizador 
     */
    public String escolherJogo(String jogos) {
        String[] arrJogos = jogos.split(";");

        for (int i = 0; i < arrJogos.length; i++) {
            System.out.println(arrJogos[i]);
        }

        System.out.println("");
        String jogo = getString("Introduza o nome do jogo que quer carregar: ");

        if (!jogoExiste(arrJogos, jogo)) {
            do {
                setMensagem("O jogo introduzido não existe!");
                jogo = getString("Introduza o nome do jogo que quer carregar: ");
            } while (!jogoExiste(arrJogos, jogo));
        }

        return jogo;
    }
    
    /**
     * @param jogos Array de Strings com o nome de todos os jogos
     * @param jogo String com o nome do jogo escolhido pelo utilizador
     * @return True se o jogo introduzido pelo utilizador existe, False se não existir
     */
    public boolean jogoExiste(String[] jogos, String jogo) {
        for (int i = 0; i < jogos.length; i++) {
            if (jogo.equals(jogos[i])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Imprime o tabuleiro
     * @param jogo Objeto Jogo com as informações do Jogo : fichas, jogadores, index de jogadores.
     */
    public void printTabuleiro(QuatroEmLinhaJogo jogo, boolean isCampeonato) {
        char[][] tabuleiro = jogo.getTabuleiro();
        System.out.println();

        System.out.println("1 2 3 4 5 6 7");
        
        for (int i = (jogo.LINHAS - 1); i >= 0; i--) {
            System.out.println(tabuleiro[i][0] + " " + tabuleiro[i][1] + " " + tabuleiro[i][2] + " " + tabuleiro[i][3] + " " + tabuleiro[i][4] + " " + tabuleiro[i][5] + " " + tabuleiro[i][6]);
        }
        
        if (!isCampeonato) {
            System.out.println();
            System.out.println("0 - Sair");
        }
    }
    
    /**
     * @param jogo Objeto Jogo com as informações do Jogo : fichas, jogadores, index de jogadores. 
     * @return A coluna que o utilizador selecionou para jogar
     */
    public int getJogada(QuatroEmLinhaJogo jogo) {
        char[][] tabuleiro = jogo.getTabuleiro();
        System.out.println();
        
        int col = getInt(jogo.getJogador().getNome() + ", digite o número da coluna em que quer jogar? ");

        if (col == 0) {
            return col; 
        } else {
            while ((col < 1) || (col > jogo.COLUNAS) || (tabuleiro[5][col - 1] != '.')) {
                System.out.println("Coluna inválida. Tente novamente.");
                col = getInt(jogo.getJogador().getNome() + ", digite a coluna em que quer jogar? ");
            }
        }
   
        return col; 
    }
    
    /**
     * Devolve jogada efetuada num jogo do campeonato.
     * @param jogo
     * @return 
     */
    public int getJogadaCampeonato(QuatroEmLinhaJogo jogo) {
        char[][] tabuleiro = jogo.getTabuleiro();
        System.out.println();
        
        int col = getInt(jogo.getJogador().getNome() + ", digite o número da coluna em que quer jogar? ");

        while ((col < 1) || (col > jogo.COLUNAS) || (tabuleiro[5][col - 1] != '.')) {
            System.out.println("Coluna inválida. Tente novamente.");
            col = getInt(jogo.getJogador().getNome() + ", digite a coluna em que quer jogar? ");
        }
   
        return col; 
    }
    
    /**
     * @param msg String
     * @return A String que lê, ou retorna "" se der catch numa excepção
     */
    public String getString(String msg) {
        System.out.print(msg);
        try {
            return breader.readLine();
        } catch (IOException ex) {
            setMensagemErro(ex.toString());
            return "";
        }
    }
    
    /**
     * @param msg String
     * @return Retorna um int, introduzido pelo utilizador
     */
    public int getInt(String msg) {
        int retValue = 0;
        boolean isInt = false;

        System.out.print(msg);
        
        while (!isInt) {
            try {
                retValue = scanner.nextInt();
                isInt = true;
            } catch (InputMismatchException ex) {
                scanner.next();
                System.out.println("Número inválido. Introduza um número inteiro.");
                System.out.print(msg);
            }
        }
  
        return retValue;
    }
    
    /**
     * @param msg String
     * @return A opcção do utilizador
     */
    public boolean getSimNao(String msg) {
        char opcao = ' ';
        boolean isSimNao = false;

        System.out.print(msg);
        
        while (!isSimNao) {
            try {
                opcao = scanner.next().toLowerCase().charAt(0);
                while (opcao != 's' && opcao != 'n') {
                    System.out.println("Opção inválida. Introduza uma opção válida.");
                    System.out.print(msg);
                    opcao = scanner.next().toLowerCase().charAt(0);
                }
                isSimNao = true;
            } catch (InputMismatchException ex) {
                scanner.next();
                System.out.println("Opção inválida. Introduza uma opção válida.");
                System.out.print(msg);
            }
        }
        
        return opcao == 's';
    }
    
    /**
     * Método para definir a mensagem a mostrar ao utilizador
     * @param msg String 
     */
    public void setMensagem(String msg) {
        System.out.println(msg);
    }
    
    /**
     * Método para definir a mensagem de erro a mostrar ao utilizador
     * @param msg String
     */
    public void setMensagemErro(String msg) {
        System.err.println(msg);
    }
}
