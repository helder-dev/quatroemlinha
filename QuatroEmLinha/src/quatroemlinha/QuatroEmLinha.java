/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quatroemlinha;

/**
 * @author Hélder Oliveira
 * @author Rui Rafael
 */
public class QuatroEmLinha {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        QuatroEmLinhaUI ui = new QuatroEmLinhaUI();
        QuatroEmLinhaFM fm = new QuatroEmLinhaFM();

        int opcao = 0;

        do {
            opcao = ui.menuPrincipalV2();

            switch (opcao) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    int subOpcao;

                    subOpcao = ui.menu1vs1V2();
                    switch (subOpcao) {
                        case 1:
                            // Novo Jogo
                            novoJogo1vs1(ui, fm);
                            break;
                        case 2:
                            // Continuar Jogo
                            carregarJogo1vs1(ui, fm);
                            break;
                        case 3:
                            // Voltar
                            break;
                    }
                    break;
                case 2:
                    subOpcao = ui.menuCampeonato(ui);
                    
                    switch (subOpcao) {
                        case 1:
                            novoCampeonato(fm , ui);
                            break;
                        case 2:
                            carregarJogoCampeonato(ui, fm);
                            break;
                        case 3:
                            break;
                    }
                    break;
                case 3:
                    ui.setMensagem("RANKING");
                    break;
            }
        } while (opcao != 0);
    }

    /**
     * Método utilizado para criar o jogador. Verifica se o jogador já existe nos ficheiros, se existir , pergunta se o jogador
     * o quer utilizar, se o utilizador responder "S" , retorna esse jogador.
     * @param ui Objeto User Interface
     * @param fm Objeto File Manager
     * @param numJogador Número do Jogador
     * @return Jogador
     */
    public static Jogador criarJogador(QuatroEmLinhaUI ui, QuatroEmLinhaFM fm, int numJogador) {
        int idJogador = ui.getInt("Introduza o número do " + numJogador + "º jogador: ");

        while (fm.jogadorExiste(idJogador)) {
            if (ui.getSimNao("O jogador " + idJogador + " já existe. Deseja utiliza-lo? (S/N)")) {
                return fm.getJogador(idJogador);
            } else {
                idJogador = ui.getInt("Introduza o número do " + numJogador + "º jogador: ");
            }
        }

        String nomeJogador = ui.getString("Introduza o nome do " + numJogador + "º jogador: ");
        int idadeJogador = ui.getInt("Introduza a idade do " + numJogador + "º jogador: ");

        Jogador jogador = new Jogador(idJogador, nomeJogador, idadeJogador, 0, 0, 0, 0, 0, 0);
        fm.guardarJogador(jogador);

        return jogador;
    }

    /**
     * Método para jogar no modo 1vs1.
     * @param ui Objeto User Interface
     * @param fm Objeto File Manager
     */
    public static void novoJogo1vs1(QuatroEmLinhaUI ui, QuatroEmLinhaFM fm) {
        Jogador[] jogadores = new Jogador[2];
        jogadores[0] = criarJogador(ui, fm, 1);
        jogadores[1] = criarJogador(ui, fm, 2);

        QuatroEmLinhaJogo jogo = new QuatroEmLinhaJogo(fm.getNumJogo(), jogadores);
        ui.printTabuleiro(jogo, false);

        while (!jogo.isGameOver()) {
            int coluna = ui.getJogada(jogo);
            if (coluna != 0) {
                jogo.aplicarJogada(coluna);
                jogo.setProximoJogador();
                ui.printTabuleiro(jogo, false);
            } else {
                if (ui.getSimNao("Deseja gravar o jogo? (S/N)")) {
                    fm.guardarJogo(jogo);
                }
                break;
            }
        }

        if (jogo.isTabuleiroCheio()) {
            jogo.getJogador().setNumEmpates(1);
            //fm.atualizarJogador(jogo.getJogador());
            jogo.setProximoJogador();
            jogo.getJogador().setNumEmpates(1);
            //fm.atualizarJogador(jogo.getJogador());
            
            ui.setMensagem("Jogo Empatado!");
        } else if (jogo.isGameOver()) {
            jogo.getJogador().setNumDerrotas(1);
            //fm.atualizarJogador(jogo.getJogador());
            jogo.setProximoJogador();
            
            jogo.getJogador().setNumJogosGanhos(1);
            //fm.atualizarJogador(jogo.getJogador());
            
            ui.setMensagem("O jogador " + jogo.getJogador().getNome() + " ganhou!");
        }
    }
    
    
    /**
     * Método utilizado para quando o jogador quer continuar um jogo que está guardado nos ficheiros.
     * Mostra a lista dos jogos guardados, pede ao utilizador para escrever o nome do jogo que quer continuar 
     * e carrega o jogo. Se não houver jogos, aparece a mensagem: "Não existem jogos guardados".
     * @param ui Objeto User Interface
     * @param fm Objeto File Manager
     */
    public static void carregarJogo1vs1(QuatroEmLinhaUI ui, QuatroEmLinhaFM fm) {
        if (fm.jogosGuardados().equals("")) {
            ui.setMensagem("Não existem jogos guardados!");
        } else {
            String nomeJogo = ui.escolherJogo(fm.jogosGuardados());
            QuatroEmLinhaJogo jogo = fm.getJogoGuardado(nomeJogo);
            
            ui.printTabuleiro(jogo, false);

            while (!jogo.isGameOver()) {
                int coluna = ui.getJogada(jogo);
                if (coluna != 0) {
                    jogo.aplicarJogada(coluna);
                    jogo.setProximoJogador();
                    ui.printTabuleiro(jogo, false);
                } else {
                    if (ui.getSimNao("Deseja gravar o jogo? (S/N)")) {
                        fm.guardarJogo(jogo);
                    }
                    break;
                }
            }

            if (jogo.isTabuleiroCheio()) {
                System.out.println("Empate!");
            } else if (jogo.isGameOver()) {
                System.out.println("Ganhou!");
            }
        }

    }
    
    /**
     * Método para criar o campeonato. Pergunta o número de jogadores do campeonato, cria os jogadores para o campeonato , inicia
     * os jogos do Campeonato e mostra quem foi o vencedor do campeonato.
     * @param fm Objeto File Manager
     * @param ui Objeto User Interface
     */
    public static void novoCampeonato(QuatroEmLinhaFM fm,QuatroEmLinhaUI ui){
        int numJogadores = ui.getInt("Quantos jogadores quer no campeonato: ");
        while(numJogadores <= 2 ){
            System.out.println("O número minímo de jogadores para um campeonato é 3. Introduza novamente");
            numJogadores = ui.getInt("");
        }
        
        Jogador[] jogadoresCampeonato = new Jogador[numJogadores];
        
        for(int i = 0 ; i < numJogadores ; i++){
            jogadoresCampeonato[i] = criarJogador(ui, fm, (i + 1));
        }
        
        QuatroEmLinhaCampeonato camp = new QuatroEmLinhaCampeonato(jogadoresCampeonato);
        
        while(camp.fimCampeonato() == false){
            int z = 1;
            
            QuatroEmLinhaJogo jogo = new QuatroEmLinhaJogo(z,camp.getJogadoresCampeonato());
            ui.printTabuleiro(jogo, true);

            while (!jogo.isGameOver()) {
                int coluna = ui.getJogada(jogo);
                jogo.aplicarJogada(coluna);
                jogo.getJogador().setTotalJogadasCampeonato(1);
                jogo.setProximoJogador();
                ui.printTabuleiro(jogo, true);
            }

            if (jogo.isTabuleiroCheio()) {
                System.out.println("Empate!");
                Jogador jogador1Empate = jogo.getJogador();
                jogo.setProximoJogador();
                Jogador jogador2Empate = jogo.getJogador();
                camp.atualizarCalendario(jogador1Empate, jogador2Empate , "Empate");
            } else if (jogo.isGameOver()) {
                Jogador loser = jogo.getJogador();
                jogo.setProximoJogador();
                Jogador winner = jogo.getJogador();                
                camp.atualizarCalendario(winner, loser , "Vencedor");
                System.out.println("O jogador : " + winner.getNome() + " ganhou!");
            }
            
            if(camp.fimCampeonato() == false){
                boolean continuar = ui.getSimNao("Deseja continuar o campeonato? (S/N)");
                if (continuar) {
                    System.out.println("Próximo Jogo..");
                } else {
                    if (ui.getSimNao("Deseja gravar o campeonato? (S/N)")) {
                        if (fm.ficheiroExiste("Campeonato.txt")) {
                            ui.setMensagem("Já existe um campeonato guardado!");
                        } else {
                            fm.guardarCampeonato(camp);
                        }
                    }
                    break;
                }
            }
            
            z++;
        }
        
        if(camp.fimCampeonato() == true) {
            System.out.println("");
            System.out.println("O jogador : " + camp.determinarVencedorCampeonato().getNome() + " venceu o campeonato.");
            System.out.println("Com : " + camp.determinarVencedorCampeonato().getTotalPontosCampeonato() + " Pontos");
            System.out.println("      " + camp.determinarVencedorCampeonato().getNumJogosGanhos() + " Vitória/s");
            System.out.println("      " + camp.determinarVencedorCampeonato().getNumEmpates() + " Empate/s");
            System.out.println("      " + camp.determinarVencedorCampeonato().getNumDerrotas() + " Derrota/s");
            System.out.println("      " + camp.determinarVencedorCampeonato().getTotalJogadasCampeonato() + " Jogada/s");
        }
    }
    
    /**
     * Método para carregar o campeonato guardado no ficheiro.
     * @param ui
     * @param fm 
     */
    public static void carregarJogoCampeonato(QuatroEmLinhaUI ui, QuatroEmLinhaFM fm) {
        if (!fm.ficheiroExiste("Campeonato.txt")) {
            ui.setMensagem("Não existem campeonatos guardados!");
        } else {
            QuatroEmLinhaCampeonato camp = fm.getCampeonatoGuardado();

            while(camp.fimCampeonato() == false){
                int z = 1;

                QuatroEmLinhaJogo jogo = new QuatroEmLinhaJogo(z,camp.getJogadoresCampeonato());
                ui.printTabuleiro(jogo, true);

                while (!jogo.isGameOver()) {
                    int coluna = ui.getJogada(jogo);
                    jogo.aplicarJogada(coluna);
                    jogo.getJogador().setTotalJogadasCampeonato(1);
                    jogo.setProximoJogador();
                    ui.printTabuleiro(jogo, true);
                }

                if (jogo.isTabuleiroCheio()) {
                    System.out.println("Empate!");
                    Jogador jogador1Empate = jogo.getJogador();
                    jogo.setProximoJogador();
                    Jogador jogador2Empate = jogo.getJogador();
                    camp.atualizarCalendario(jogador1Empate, jogador2Empate , "Empate");
                } else if (jogo.isGameOver()) {
                    Jogador loser = jogo.getJogador();
                    jogo.setProximoJogador();
                    Jogador winner = jogo.getJogador();                
                    camp.atualizarCalendario(winner, loser , "Vencedor");
                    System.out.println("O jogador : " + winner.getNome() + " ganhou!");
                }

                if(camp.fimCampeonato() == false){
                    boolean continuar = ui.getSimNao("Deseja continuar o campeonato? (S/N)");
                    if (continuar) {
                        System.out.println("Próximo Jogo..");
                    } else {
                        if (ui.getSimNao("Deseja gravar o campeonato? (S/N)")) {
                            if (fm.ficheiroExiste("Campeonato.txt")) {
                                ui.setMensagem("Já existe um campeonato guardado!");
                            } else {
                                fm.guardarCampeonato(camp);
                            }
                        }
                        break;
                    }
                }

                z++;
            }

            if(camp.fimCampeonato() == true) {
                System.out.println("");
                System.out.println("O jogador : " + camp.determinarVencedorCampeonato().getNome() + " venceu o campeonato.");
                System.out.println("Com : " + camp.determinarVencedorCampeonato().getTotalPontosCampeonato() + " Pontos");
                System.out.println("      " + camp.determinarVencedorCampeonato().getNumJogosGanhos() + " Vitória/s");
                System.out.println("      " + camp.determinarVencedorCampeonato().getNumEmpates() + " Empate/s");
                System.out.println("      " + camp.determinarVencedorCampeonato().getNumDerrotas() + " Derrota/s");
                System.out.println("      " + camp.determinarVencedorCampeonato().getTotalJogadasCampeonato() + " Jogada/s");
            }
        }
    }
}